-- Load files
local modpath = minetest.get_modpath(minetest.get_current_modname())

dofile(modpath.."/blast_furnace.lua") -- Load Blast Furnaces
dofile(modpath.."/furnace.lua") -- Load Furnaces
dofile(modpath.."/smoker.lua") -- Load Smokers